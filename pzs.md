Руководство по написанию ПЗ и других отчетов
============================================

LaTeX код отчетов по проектам следует хранить в соответсвующих репозиториях на в
нашей группе GitLab. За счет GitLab CI автоматизируется сборка целевого
документа и проверка наличия ошибок. Также рекомендуется использовать механизм
Merge Request (MR) для обсуждения изменений.

Репозитории для основных отчетов создаются автоматизированно на основе шаблона
[skibcsit/thesis-template](https://gitlab.com/skibcsit/thesis-template/).

Общие правила для репозиториев отчетов:

- на каждый проект (семестр или практику) создается отдельный репозиторий
    - каждый репозиторий создается клонированием thesis-template
    - если требуется перенести в новый отчет материалы предыдущих отчетов, это
      следует сделать вручную
- промежуточные документы по проекту, такие как РСПЗ, следует хранить в общем
  репозитории отчета
- по возможности, следует избегать ветвления и хранить текущую версию отчета в
  ветви master
    - отдельные документы (РСПЗ, а также версии ПЗ) следует помечать тэгами
      вместо создания отдельных ветвей
    - в основном, ветви следует использовать только для создания MR, с целью их
      скорейшего вливания в master
    - следует, по возможности, сохранять линейную и ясную историю — использовать
      операцию Rebase вместо Merge при соединении ветвей и группировать коммиты
      по смыслу за счет операции squash
